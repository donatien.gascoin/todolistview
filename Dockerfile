# build
FROM node:9.11.1-alpine as build-stage

WORKDIR /app

COPY package*.json ./

RUN apk update \
    && apk add --virtual build-dependencies \
        git \
    && npm install

COPY . .

RUN npm run build

# production
FROM nginx:alpine as production-stage

COPY --from=build-stage /app/dist /usr/share/nginx/html

RUN rm -rf /etc/nginx/conf.d/*

COPY --from=build-stage /app/nginx.default.conf /etc/nginx/conf.d

EXPOSE 8080

CMD ["nginx", "-g", "daemon off;"]
