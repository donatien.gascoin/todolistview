import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import router from './router'
import store from './store/store'
import VueJWT from 'vuejs-jwt'
import VueResource from 'vue-resource'
import Notifications from 'vue-notification'
import velocity from 'velocity-animate'

Vue.use(VueJWT)
Vue.use(Notifications, { velocity })
Vue.use(VueResource)
Vue.http.interceptors.push((request, next) => {
  request.headers.set('Authorization', localStorage.getItem('header'))
  next()
})

const moment = require('moment')
Vue.use(require('vue-moment'), {
  moment
})

Vue.prototype.$window = window

Vue.config.productionTip = false

new Vue({
  el: '#app',
  http: {
    root: '/'
  },
  router,
  store,
  render: h => h(App)
}).$mount('#app')
