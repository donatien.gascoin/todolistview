import Vue from 'vue'
import Router from 'vue-router'

import home from '@/views/Home.vue'
import logout from '@/views/Logout.vue'
import login from '@/views/Login.vue'

import store from './store/store.js'

Vue.use(Router)

let router = new Router({
  linkActiveClass: 'active',
  linkExactActiveClass: 'active',
  base: process.env.BASE_URL,
  mode: 'hash',
  routes: [
    {
      path: '/home',
      name: 'Home',
      component: home,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/logout',
      name: 'Logout',
      component: logout,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/login',
      name: 'Login',
      component: login,
      meta: {
        guest: true
      }
    },
    { path: '*', redirect: { name: 'Login' } }
  ]
})

router.beforeEach((to, from, next) => {
  store.dispatch('load')
  const loggedIn = store.state.auth.isAuth
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!loggedIn) {
      next({ name: 'Login' })
    } else {
      next()
    }
  } else if (to.matched.some(record => record.meta.guest)) {
    if (loggedIn) {
      next({ name: 'Home' })
    } else {
      next()
    }
  } else if (to.matched.some(record => record.meta.notFound)) {
    next()
  }
})

export default router
