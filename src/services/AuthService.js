import Vue from 'vue'

export default class AuthService {
  signin (form) {
    return Vue.http.post('https://todo.donatien-gascoin.fr/api/auth/signin', form)
  }

  signup (form) {
    return Vue.http.post('https://todo.donatien-gascoin.fr/api/auth/signup', form)
  }

  checkEmailAvailability (email) {
    return Vue.http.get('https://todo.donatien-gascoin.fr/api/user/checkEmailAvailability/' + email)
  }

  getToken () {
    if (this.isToken()) {
      return Vue.$jwt.getToken()
    }
    return null
  }

  isToken () {
    return Vue.$jwt.hasToken()
  }

  logout () {
    // Remove token
    localStorage.clear()
    location.reload()
  }

  decodeToken () {
    var data = Vue.$jwt.decode(this.getToken())
    /* console.log(data)
    localStorage.setItem('companyId', data.companyId)
    localStorage.setItem('companyName', data.companyName)
    localStorage.setItem('firstName', data.firstName)
    localStorage.setItem('lastName', data.lastName)
    localStorage.setItem('userId', data.sub)
    localStorage.setItem('email', data.email)
    localStorage.setItem('role', data.role) */
    return data
  }

  storeToken (token) {
    localStorage.setItem('auth_token', token.accessToken)
    localStorage.setItem('header', token.tokenType + ' ' + token.accessToken)
  }

  hasRole () {
    return true
  }
}
