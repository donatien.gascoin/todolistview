import Vue from 'vue'
import store from '@/store/store'

export default class ErrorService {
  getServeurNotif (status, statusMsg) {
    var data = {
      group: 'alert',
      type: 'error',
      duration: 6000,
      text: ''
    }

    if (status === 500 || status === 500 || status === 502 || status === 503 || status === 504) {
      store.commit('alert/serverUnreachable', true)
    } else if (status === 401) {
      // Remove token
      store.dispatch('alert/error', { content: 'Action unauthorized' })
      store.dispatch('logout')
      Vue.$router.push({ name: 'login' })
    } else {
      console.log(statusMsg)
      var msg = JSON.parse(statusMsg)
      if (Array.isArray(msg.fields)) {
        msg.fields.forEach(e => {
          store.dispatch('alert/error', { content: Vue.prototype.$vuetify.t('$vuetify.form.errors.' + e) })
        })
      } else {
        switch (msg.errorCode) {
          case 'UNKNOWN_USER' :
            data.text = 'User unknown'
            break
          case 'UNKNOWN_TASKLIST' :
            data.text = 'List unknown'
            break
          case 'UNKNOWN_TASK' :
            data.text = 'Task unknown'
            break
          case 'BAD_CREDENTIALS' :
            data.text = 'Bad credential'
            break
          case 'INSUFFICIENT_PERMISSION' :
            data.text = 'You can\'t perform this action'
            break
          case 'EMAIL_ALREADY_TAKEN' :
            data.text = 'Mail already taken'
            break
          case 'LOGIN_ALREADY_TAKEN' :
            data.text = 'Login already taken'
            break
          case 'WRONG_CAPTCHA' :
            data.text = 'ReCaptcha didn\'t work. Please refresh the page and try again'
            break
          default:
            data.text = 'Unknown error'
        }
        this.sendCustomNotif(data)
      }
    }
  }

  sendCustomNotif (data) {
    Vue.notify(data)
  }

  sendNotif (type, title, msg) {
    Vue.notify({
      group: 'alert',
      type: type,
      title: title,
      text: msg
    })
  }
}
