import Vue from 'vue'

export default class UserService {
  getList (id) {
    return Vue.http.get('https://todo.donatien-gascoin.fr/api/list/' + id)
  }

  addList (form) {
    return Vue.http.post('https://todo.donatien-gascoin.fr/api/list/add', form)
  }

  editList (form) {
    return Vue.http.put('https://todo.donatien-gascoin.fr/api/list/edit', form)
  }

  removeList (listId) {
    return Vue.http.delete('https://todo.donatien-gascoin.fr/api/list/remove/' + listId)
  }
}
