import Vue from 'vue'

export default class UserService {
  addTask (form) {
    return Vue.http.post('https://todo.donatien-gascoin.fr/api/task/add', form)
  }

  editTask (form) {
    return Vue.http.put('https://todo.donatien-gascoin.fr/api/task/edit', form)
  }

  removeTask (listId, taskId) {
    return Vue.http.delete('https://todo.donatien-gascoin.fr/api/task/remove/' + listId + '/' + taskId)
  }
}
