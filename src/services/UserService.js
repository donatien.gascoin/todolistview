import Vue from 'vue'

export default class UserService {
  getUser () {
    return Vue.http.get('https://todo.donatien-gascoin.fr/api/user')
  }

  updateShowDescription (show) {
    return Vue.http.get('https://todo.donatien-gascoin.fr/api/user/description?show=' + show)
  }

  checkUser (form) {
    return Vue.http.post('https://todo.donatien-gascoin.fr/api/user/check', form)
  }

  resetPassword (form) {
    return Vue.http.put('https://todo.donatien-gascoin.fr/api/user/resetPassword', form)
  }
}
