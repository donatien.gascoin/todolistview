const state = {
  messages: [],
  loading: true,
  serverUnreachable: false
}

const mutations = {
  message (state, message) {
    state.messages.push(message)
  },
  clear (state) {
    state.messages = []
  },
  logout (state) {
    state.messages.push({ type: 'success', content: 'You have been successfully disconnected !' })
  },
  serverUnreachable (state, status) {
    state.serverUnreachable = status
  }

}
const getters = {
  getNumber: state => () => state.messages.length,
  getMessages: state => state.messages,
  getServerUnreachable: state => state.serverUnreachable
}

const actions = {
  success ({ commit }, { title, content }) {
    commit('message', { type: 'success', title: title, content: content })
  },
  warning ({ commit }, { title, content }) {
    commit('message', { type: 'warning', title: title, content: content })
  },
  error ({ commit }, { title, content }) {
    commit('message', { type: 'error', title: title, content: content })
  },
  clear ({ commit }) {
    commit('clear')
  }
}

export const alert = {
  namespaced: true,
  state: state,
  mutations: mutations,
  getters: getters,
  actions: actions
}
