import AuthService from '@/services/AuthService'

const authService = new AuthService()

const state = {
  isAuth: false,
  accessToken: null
}

const mutations = {
  load (state) {
    state.isAuth = !!localStorage.getItem('auth_token')
    state.accessToken = localStorage.getItem('auth_token')
  },
  logout (state) {
    state.status = {}
    state.accessToken = null
    state.isAuth = false
  }

}
const getters = {
  authenticated: state => state.isAuth,
  token: state => state.accessToken
}

const actions = {
  authenticate ({ dispatch }, accessToken) {
    // Store datas
    authService.storeToken(accessToken)
    dispatch('load', null, { root: true })
  }
}

export const auth = {
  namespaced: true,
  state: state,
  mutations: mutations,
  getters: getters,
  actions: actions
}
