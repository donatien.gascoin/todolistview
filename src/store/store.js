import Vue from 'vue'
import Vuex from 'vuex'

import { auth } from './auth.store'
import { user } from './user.store'
import { alert } from './alert.store'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    auth,
    user,
    alert
  },

  actions: {
    load ({ commit }) {
      commit('auth/load')
      commit('user/load')
    },
    logout ({ commit }) {
      localStorage.clear()
      commit('clearAll')
      commit('auth/logout')
      commit('user/logout')
      commit('alert/logout')
    }
  },
  state: {
    loading: false,
    listToLoad: null,
    loadNewList: false,
    addList: false,
    listToEdit: null,
    editList: false,
    showDescription: true,
    addTask: false,
    deleteCurrent: false
  },
  getters: {
    isLoading: state => state.loading,
    listToLoad: state => state.listToLoad,
    loadNewList: state => state.loadNewList,
    addList: state => state.addList,
    listToEdit: state => state.listToEdit,
    editList: state => state.editList,
    showDescription: state => state.showDescription,
    addTask: state => state.addTask,
    isMobile: () => {
      return Vue.prototype.$vuetify.breakpoint.name === 'xs'
    },
    deleteCurrent: state => state.deleteCurrent
  },

  mutations: {
    loading (state, status) {
      state.loading = status
    },
    changeList (state, list) {
      state.listToLoad = list
      state.loadNewList = true
    },
    newListLoaded (state, list) {
      state.listToLoad = list
      state.loadNewList = false
    },
    addList (state, s) {
      state.addList = s
    },
    editList (state, list) {
      state.listToEdit = list
      state.editList = true
    },
    listEdited (state) {
      state.editList = false
    },
    showDescription (state, bool) {
      state.showDescription = bool
    },
    addTask (state) {
      state.addTask = !state.addTask
    },
    changeNbCompletedTaskToListToLoad (state, value) {
      state.listToLoad.nbCompletedTask += value
    },
    deleteCurrent (state, value) {
      state.deleteCurrent = value
    },
    clearAll (state) {
      state.listToLoad = null
      state.loadNewList = false
      state.addList = false
      state.listToEdit = null
      state.editList = false
      state.showDescription = true
      state.addTask = false
      state.deleteCurrent = false
    }
  }

})

export default store
