import AuthService from '@/services/AuthService'

const authService = new AuthService()

const state = {
  userInfo: null,
  list: null
}

const mutations = {
  load (state) {
    state.userInfo = authService.decodeToken(localStorage.getItem('auth_token'))
  },
  updateList (state, list) {
    state.list = list
  },
  update (state, user) {
    state.userInfo = user
  },
  logout (state) {
    state.userInfo = null
  },
  changeCompanyName (state, name) {
    state.companyName = name
  }
}
const getters = {
  userInfo: state => state.userInfo,
  getName: state => state.userInfo ? state.userInfo.name : null,
  getMail: state => state.userInfo ? state.userInfo.mail : null,
  getId: state => state.userInfo ? state.userInfo.id : null,
  getList: state => state.list
}

const actions = {
  info ({ commit }, user) {
    commit('update', user)
  }

}

export const user = {
  namespaced: true,
  state: state,
  mutations: mutations,
  getters: getters,
  actions: actions
}
