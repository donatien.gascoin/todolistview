module.exports = {
  devServer: {
    overlay: {
      warnings: true,
      errors: true
    },
    host: '0.0.0.0',
    port: 8070, // CHANGE YOUR PORT HERE!
    https: false,
    hotOnly: false,
    proxy: {
      '^/api': {
        target: 'http://localhost:8071/',
        changeOrigin: true
      }
    }

  },
  lintOnSave: process.env.NODE_ENV !== 'production',
  publicPath: '/'
}
